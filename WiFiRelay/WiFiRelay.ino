/*
    Implemented over initial work of Mischianti Renzo <https://www.mischianti.org>
*/

#include "Arduino.h"
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

#define RELAY 0 // relay connected to  GPIO0
#define INFOLED 1 // Led on board connected to GPIO1

const char* ssid = "ssid";
const char* password = "password";
const char* device_name = "boiler";

ESP8266WebServer server(80);

uint8_t current_state = LOW;
void SetupOutput() {
  pinMode(RELAY, OUTPUT);
  pinMode(INFOLED, OUTPUT);
  SetOutput(current_state);
}

int wifiStatus = WL_IDLE_STATUS;
void SetupWiFi() {
  WiFi.mode(WIFI_STA);
  wifiStatus = WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (wifiStatus != WL_CONNECTED) {
    delay(1000);
    wifiStatus = WiFi.status();
    Serial.print("wifiStatus: ");
    Serial.println(wifiStatus);
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  // Activate mDNS this is used to be able to connect to the server
  // with local DNS hostmane esp8266.local
  if (MDNS.begin(device_name)) {
    Serial.print("MDNS responder started as ");
    Serial.println(device_name);
  }
}

void SetupServer() {
  // Set server routing
  restServerRouting();
  // Set not found response
  server.onNotFound(handleNotFound);
  // Start server
  server.begin();
  Serial.println("HTTP server started");
}

void setup(void) {
  Serial.begin(115200);
  SetupOutput();
  SetupWiFi();
  SetupServer();
}

void SetOutput(uint8_t val) {
  digitalWrite(RELAY, val);
  digitalWrite(INFOLED, val);
  current_state = val;
}

void enable() {
  SetOutput(LOW);
  server.send(200, "text/json", "{\"Status\": \"ON\"}");
}

void disable() {
  SetOutput(HIGH);
  server.send(200, "text/json", "{\"Status\": \"OFF\"}");
}

void currentStatus() {
  if (current_state == LOW) {
    server.send(200, "text/json", "{\"Status\": \"ON\"}");
  } else {
    server.send(200, "text/json", "{\"Status\": \"OFF\"}");
  }
}

// Define routing
void restServerRouting() {
  //  server.on("/", HTTP_GET, []() {
  //    server.send(200, F("text/html"),
  //                F("Welcome to the REST Web Server"));
  //  });
  server.on(F("/"), HTTP_GET, currentStatus);
  server.on(F("/on"), HTTP_GET, enable);
  server.on(F("/off"), HTTP_GET, disable);
}

// Manage not found URL
void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void loop(void) {
  server.handleClient();
}
